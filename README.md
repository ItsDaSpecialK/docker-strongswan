docker strongswan
============

# Quickstart

## Get initialize configuration:

```bash
docker create itsdaspecialk/strongswan
docker cp $(docker ps -ql):/etc/strongswan .
docker rm $(docker ps -ql)
```

### Get help for config file
[Tutorial for setting up IKEv2](https://hub.zhovner.com/geek/universal-ikev2-server-configuration/)

**NOTE:** I recommend not using EAP in ipsec.secrets, since you should never store passwords in plaintext. I personally use NTLM hashes in the format:
`testuser : NTLM 0xD32A2901011176349B41D406DCC95A90`

# Start the container 

**NOTE:** Priveleged mode is required, otherwise you wont be able to connect.

```bash
docker run -d --net=host --privileged \
  -v $(pwd)/strongswan:/etc/strongswan itsdaspecialk/strongswan
```

## Using Letsencrypt Certs

 - place `chain.pem` in `ipsec.d/cacerts/`
 - place `fullchain.pem` in `ipsec.d/certs/`
 - place `privkey.pem` in `ipsec.d/private/`
 
# Updating ipsec.secrets

Edit the file and then run the following command:
```bash
docker exec -it <container_name> strongswan secrets
```

## Issues

If you have an issue, please create an [issue on Bitbucket](https://bitbucket.org/ItsDaSpecialK/docker-strongswan/issues)

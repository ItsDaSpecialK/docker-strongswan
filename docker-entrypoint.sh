#!/bin/bash
set -e

if [ "${1:0:1}" = '-' ]; then
	set -- strongswan start "--nofork" "$@"
fi

if [ "$#" = 0 ]; then
	set -- strongswan start "--nofork"
fi

if [ "$1" = "strongswan" ]; then
	# If strongswan didn't exit cleanly, and left a pid file,
	# then delete it.
	if [ -f "/var/run/starter.charon.pid" ]; then
		rm /var/run/starter.charon.pid
	fi
	exec "$@"
fi

exec "$@"
